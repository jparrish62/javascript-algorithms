const functions = {
	add: (num1, num2) => num1 + num2,

	isFibonacci: (num) => {
	  const fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811]
		return fibonacci.includes(num)
	}, 

	runLength: (str) => {
		obj = {} 
		arr = []
		strArray = str.split('')
		strArray.forEach( e => {
      obj[e] = str.split(e).length -1
		})
		for (let [key, value] of Object.entries(obj)) {
      arr.push(`${value}` + `${key}`);
    }
		return arr.join('')
	},

	consecutive: (arr) => {
		let new_array = []
		sorted_array = arr.sort((a, b) => a - b)
		const first = sorted_array[0]
		const last = sorted_array[arr.length -1]
		for(let i = first; i < last; i++) {
      new_array.push(i)
		}

		for(let i = 0; i < arr.length; i++) {
      new_array.splice(arr.indexOf(arr[i]), 1)
		} 
		return new_array.length + 1
	}, 

	cyclicRotation: (arr, k) => {
    let count = 1 
	  let e = ""	
		
	  while (count <= k) {
			e = arr.pop()
			arr.unshift(e)
			count = count + 1
		}
		if (arr[0] == undefined) {
			return []
		}else {
			return arr
		}
	},

	oddOccurances: (arr) => {
		let odd = ''
	  let result = arr.filter(i => )
		console.log(result.length)
	}, 
  
	countingElements: (arr, x) => {
    const obj = {}
		let seconds = 0
		let cross = ''
		arr.forEach(e => {
			obj[seconds] = e
			seconds = seconds + 1
		})
		for (let [key, value] of Object.entries(obj)) {
			if (value == x) {
				cross = key
			}
		}
		return parseInt(cross)
	} 
}

module.exports = functions







