functions = require('./function')

test('Add two plus two to equal four', () => {
  expect(functions.add(2, 2)).toEqual(4)
})

test('Determines whether number being passed in is the fibonacci sequence', () => {
	const num = 34 
	expect(functions.isFibonacci(num)).toEqual(true)
})

test('Determines whether number being passed in is the fibonacci sequence', () => {
	const num = 54
	expect(functions.isFibonacci(num)).toEqual(false)
})

test("", () => {
	const str = "wwwggopp"
	expect(functions.runLength(str)).toEqual('3w2g1o2p')	
})

test("creates an array of consecutive numbers", () => {
	const arr = [5, 10, 15]
	expect(functions.consecutive(arr)).toEqual(8)
})

test("creates an array of consecutive numbers", () => {
	const arr = [-2, 10, 4]
	expect(functions.consecutive(arr)).toEqual(10)
})

test("creates an array of consecutive numbers", () => {
  const arr =	[1,5,9,10,11,12,14]
	expect(functions.consecutive(arr)).toEqual(7)
})

test("when an array is passed in it rotates array k number of times", () => {
	let arr = [3, 8, 9, 7, 6]
	let k = 3
	let arr1 = [0, 0, 0]
	let k1 = 1 
	let arr2 = [1, 2, 3, 4]
	let k2 = 4 
	const arr3 = []
	const k3 = 1
	expect(functions.cyclicRotation(arr, k)).toEqual([9, 7, 6, 3, 8])
	expect(functions.cyclicRotation(arr1, k1)).toEqual([0, 0, 0])
	expect(functions.cyclicRotation(arr2, k2)).toEqual([1, 2, 3, 4])
	expect(functions.cyclicRotation([], k3)).toEqual([])
})
 
test("when an array of element are passed in it returns unpaired element", () => {
	const arr = [9, 3, 9, 3, 9, 7, 9]
	const arr1 = [9, 3, 7, 9, 3, 9, 7]
	expect(functions.oddOccurances(arr1)).toEqual(9)
	expect(functions.oddOccurances(arr)).toEqual(7)
})

test("when an array is passed in it returns the earliest time leaves appear in every position", ()=> {
  const arr = [1, 3, 1, 4, 2, 3, 5, 4]
	const x = 5
	expect(functions.countingElements(arr, x)).toEqual(6)
})










